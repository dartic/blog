---
home: true
heroImage: /madz_logo.png
actionText: Accéder au blog →
actionLink: /blog/
features:
- title: Dévelopeur web
  details: Je suis développeur web chez Makina Corpus, full JavaScript. Front, avec Vue.js ou React, Back avec node.js et Loopback, FeatherJS.
- title: Artisan du libre
  details: Je fabrique des logiciels grâce à un savoir-faire, un faire-savoir et un savoir-être en constante évolution. Je n'utilise que des briques libres et je contribue à mon échelle.
- title: Remue-méninges
  details: Créativité, imagination, remise en question, agitateur, un peu fou aussi (mad)... J'aime proposer mes idées, comprendre celles des autres, et fabriquer avec cet écosystème.
---

<madz-blog-index />

<style lang="stylus">

@media(max-width: $MQMobile) {
  .home {
    padding-left: 0.5rem !important;
    padding-right: 0.5rem !important;
  }
}
.home .hero {
  box-shadow: $boxShadowDefault;
  background-color: white;
  padding: 1rem;
  margin-top: 1rem;
  border-radius: 2px;
}
.home .features {
  box-shadow: $boxShadowDefault;
  background-color: white;
  border-top: unset !important;
  @media(min-width: $MQMobile) {
    padding: 1rem !important;
  }
}

.home .hero .action-button {
  box-shadow: $boxShadowDefault;
  &:hover {
    box-shadow: $boxShadowEnd;
  }
}

</style>
