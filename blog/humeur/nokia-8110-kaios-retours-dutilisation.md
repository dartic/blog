---
title: Retours d'utilisation sur le Nokia 8110 avec KaiOS
cover: /images/uploads/nokia_8110_4g.jpg
cover_color: ''
description: >-
  Les smart feature phone s'intègrent de plus en plus dans le paysage des
  téléphones. Feedback sur le Nokia 8110, alias le Banana Phone, équipé de
  KaiOS...

  qui réveille notre cerveau, qui était sous perf' du smartphone !
tags:
  - kaios
  - nokia 8110
  - feedback
date: 2020-03-30T12:03:07.091Z
layout: LayoutPost
sidebar: true
author: Mathieu DARTIGUES
is_published: true
---
La sortie du Nokia 8110 4g, alias le revival du Banana Phone de Matrix
ne m'a pas laissé indifférent. J'ai fait l'acquisition l'an dernier
d'un exemplaire. Il est basé sur KaiOS, commercialisé par HMD.
Voici quelques retours.

# KaiOS

**[KaiOS](kaiostech.com/)**, c'est à la fois un **système d'exploitation** bâti  sur les ruines | restes de feu [Firefox OS](https://fr.wikipedia.org/wiki/Firefox_OS)  (l'OS créé par [Mozilla](mozilla.org/), basé sur Android et 100% web) et la **société éponyme** qui maintient cet OS, fourni le store d'application, le système de publicité, et gère ce qui peut graviter autour de l'OS.

Ce système d'exploitation équipe déjà **plus de 100 millions de téléphones**, principalement en Inde, Chine et Afrique,
avec des marques telles que Jio, Alcatel, Telma, Orange, Nokia (HMD Global).
En Inde, il est passé devant Apple en terme de part de marché.
Il est **Open Source**, son code est disponible sur [Github](https://github.com/kaiostech/).

Le 22 Juillet 2018, [Google annonçait investir 22 millions de dollars dans la société KaiOS](https://techcrunch.com/2018/06/27/google-kaios/).
On ne refuse pas l'accès aux services Google a autant d'utilisateurs,
surtout pour apprendre à les connaître et sûrement monétiser leurs données.

Début Mars, [Mozilla annonçait également leur soutien](https://www.kaiostech.com/press/kaios-technologies-and-mozilla-partner-to-enable-a-healthy-mobile-internet-for-everyone/) pour faire progresser la plate-forme.
Cela passerait par la mise à dispo des versions ESR de Firefox
(versions "entreprises" de Firefox avec mises à jour sécurité et stabilité),
à commencer par la version 78.
(à ce sujet, visionnez [la vidéo de **TechAltar** très instructive](https://www.youtube.com/watch?v=_UPk3mpcDP4))

La [société HMD Global](https://www.hmdglobal.com/)
(société finlandaise créée par des anciens de Nokia,
[mais pas que](https://www.frandroid.com/marques/nokia/404336_qui-est-hmd-lentreprise-qui-se-cache-derriere-le-retour-de-nokia))
détient les droits d'utilisation de la marque Nokia.
C'est elle qui gère la production (en sous-traitant à Foxconn)
des téléphones Android et KaiOS.

![Le vrai 8110, avec du Keanu Reeves dedans](/images/uploads/nokia_8110_matrix.png "Le vrai 8110, avec du Keanu Reeves dedans")

Tous les ingrédients sont réunis pour que le Banana Phone de Matrix 
renaisse de ses cendres !

# Le téléphone

Sorti en Juin 2018, le banana phone de la marque finlandaise HMD Global
rendu célèbre par la trilogie Matrix ressort du placard
avec une version remasterisée tournant sur KaiOS.
En 2019, je m'en procure un, avec l'intention de faire 
une petite cure "détox" de smartphone (Galaxy A3 en l'occurrence).

Il s'agit d'un **smart feature phone**, je traduis ça en
**"téléphone basique mais pas trop"**.

Voici ses caractéristiques techniques (issues du site Kaiostech) :

![Caractéristiques techniques du Nokia 8110 4G](/images/uploads/nokia_8110_4g_caracteristics.png "Caractéristiques techniques du Nokia 8110 4G")

Certes, ces caractéristiques ne vont pas faire concurrence aux smartphones.

Ce n'est pas l'idée.

Mais la **batterie** propose une durée de veille de **17 jours maximum**.
On oublierait même que cela est possible, avec une batterie de 1500 mAh.

Avec ses **512MB de RAM**, il fait parti des téléphones **"haut de gamme"**.
Il est fréquent de rencontrer des téléphones avec seulement 256MB.
Ce n'est pas précisé dans les caractéristiques, 
mais la résolution de l'écran est du **240 x 320 pixels**. Là aussi, retour en arrière !

En terme de fonctionnalités, le téléphone permet de :
* passer des appels
* envoyer des messages, les lister en mode "conversation" (*spéciale dédicasse à Julien M*), et les écrire avec le T9
* synchroniser ses emails
* disposer d'un calendrier également connecté
* surfer sur Internet avec un navigateur Firefox oldschool (version 48... et écran 240x320!)
* se géolocaliser sur une carte (google) grâce au GPS intégré
* disposer de Google Assistant ou d'autres applications type WhatsApp
* accéder à d'autres applications via le KaiStore (équivalent PlayStore / AppStore)
* et d'autres choses encore

Le téléphone dispose d'un clavier coulissant,
qui permet de répondre directement aux appels.

Une prise jack 3.5mm est présente pour la partie audio, 
ainsi qu'une prise micro USB pour recharger / brancher à un ordinateur.

# L'usage

Le premier contact est sincèrement positif,
le clavier coulisse bien,
et le téléphone se déverrouille automatiquement.
Aucun souci dans le temps, après plusieurs mois d'utilisation,
le slider fonctionne toujours correctement.

Les appels sont de bonne qualité, 
tant du côté émetteur que récepteur.
C'est quand même la fonctionnalité essentielle d'un téléphone.
Et elle est remplie.

Le menu applicatif est clair, lisible.
Vous pouvez l'afficher de deux manières, en liste ou en icônes.
Il est très facile de naviguer dedans grâce à la touche centrale.

### Le cerveau, ce composant oublié...

Habitué aux smartphones, et à leur "multi-tâche", 
il est préférable de ralentir la cadence et redevenir mono-tâche.
En effet, le système KaiOS et le dimensionnement du téléphone
font que ce n'est pas un foudre de guerre.

Au final, je trouve cette expérience plutôt reposante.
La limitation par la machine fait que vous utilisez plus votre machine personnelle,
votre cerveau, pour vous organiser, et aussi mémoriser les choses.

::: tip My 2 cents
Là où votre smartphone va vite, et vous permet d'accéder rapidement à de l'info,
votre "smart feature phone" est par nature plus lent, alors que votre cerveau, lui,
redevient "utile" et rapide pour stocker et réfléchir...
:::

### Le GPS

Au niveau des autres bons points,
le fait de pouvoir **disposer d'une carte**,
même s'il s'agit de Google Maps 
(le temps qu'une alternative type 
[Open Street Map](https://www.openstreetmap.org/) naisse),
c'est très confortable.

Le service de Google a fait l'objet d'une **adaptation pour ces petits écrans**,
et utilise au mieux les touches contextuelles de l'écran, le micro (avec Google Assistant) pour aider à la localisation.
Par contre, on ferme les yeux pour l'utilisation de nos données concédées à Google...

Oubliez pour l'instant le système de guidage.
Est disponible à l'heure actuelle uniquement la géolocalisation
et le calcul d'itinéraires.

### La saisie, clavier et T9

Quel plaisir également de retrouver un **clavier physique**.
Avec un T9 parfois buggé, certes, mais simplement le retour physique d'une touche
(on cherche pas le haptique), c'est bien !
Nous sommes des êtres analogiques ! (c'est le vieux qui parle)

Le T9 fait parfois quelques farces, 
en considèrant que j'ai tapé 2 fois sur une touche, 
alors qu'en fait non, j'ai juste tapé une fois. 
Problème hardware ou software, je ne sais pas, mais c'est parfois pénible.

Le **dictionnaire** n'est pas des mieux fournis également.
Dans mes vieux souvenirs de trentenaire, j'avais gardé l'impression que 
les dictionnaires fournis dans les téléphones des années 2000 disposaient 
d'un plus large éventail de mots.

Vous pouvez néanmoins toujours **enrichir le dictionnaire** au fur et à mesure.
Je ne sais pas s'il existe des options pour venir "sauvegarder" 
vos ajouts, si vous souhaitiez formater votre téléphone.

### Multimédia

Côté **multimédia**,
vous pouvez regarder des vidéos, mais sur un tout petit écran.
**Pratique, mais souvent illisible**.
Côté **photo**, avec un capteur de 2MP, vous ne pourrez pas faire de tirages en A3.
En même temps, cela ne fait pas partie de ses promesses.

L'absence de caméra frontale fait que nous ne pouvons pas (pour l'instant ?)
utiliser ce genre de périphérique pour des visios en famille ou entre amis.
Peut-être cet usage tendra à se développer au fur et à mesure de l'adoption
de ces téléphones, et de l'enrichissement des apps type WhatsApp.

### Google, Facebook, ...
L'investissement réalisé par Google et Facebook  permet aux utilisateurs de bénéficier de Google Maps, WhatsApp, 
Facebook et Google Assistant "by default".
Pour ma part, j'utilise beaucoup WhatsApp pour échanger avec des proches,
c'est donc très pratique.

Il y a cependant une option qui reste indisponbile sur la version KaiOS de WhatsApp,
qui permet sur les téléphones Android l'utilisation d'une version web
à partir du PC.
Cela permet par exemple de consulter et répondre aux messages WhatsApp
directement depuis son PC, via une synchronisation avec le téléphone.
Gageons que cette fonctionnalité arrivera avec une mise à jour ?

### Connectivité

Dans les autres fonctionnalités,
le fait qu'un téléphone aussi "simple" puisse se transformer en **modem USB ou WiFi**
est très pratique pour un nomade comme moi.

Vous avez votre PC avec vous, votre téléphone n'est pas super pratique pour surfer,
vous démarrez le PC et vous pouvez accéder aux ressources dont vous avez besoin
en vous connectant sur votre téléphone en USB ou WiFi (et même bluetooth ?).
Certes, cela nécessite un environnement le permettant (chez vos parents, dans le train,...)
et restreint le potentiel du téléphone en comparaison à un smartphone.

Je vois ça cependant d'un oeil positif, car cela nous éloigne de la technologie,
pour nous rappeler que nous pouvons nous en passer (si souvent... mais nous l'oublions).

Vous pouvez également **connecter vos emails** via l'application Mail,
ou un calendrier.
Concernant les **contacts**, vous pouvez **connecter** 
des comptes type **Microsoft / Google**,
mais lorsque vous souhaitez importer vos contacts depuis un fichier,
l'histoire est toute autre.
Vous devez obligatoirement passer par une carte Micro SD à intégrer
dans votre téléphone, et non en copiant votre fichier via le câble USB.
Dommage, la procédure pourrait être simplifiée.

# Verdict

J'avais l'habitude d'utiliser un Galaxy A3 2016.
Android et écran tactile donc.
Mon intention initiale était de faire un peu de détox.

Basculer sur un téléphone avec écran non tactile, de 240 x 320 pixels,
forcément, ça a piqué un peu.
Par contre, l'objectif est largement atteint.

Avec un seul téléphone sous KaiOS à mon actif, 
il m'est difficile de distinguer l'usage du système KaiOS du téléphone lui-même.

<table style="text-align: left; margin: 1rem auto;">
  <tr>
    <th><strong>Les plus</strong></th>
    <th><strong>Les moins</strong></th>
  </tr>
  <tr>
    <td>
      <ul>
        <li>durée de vie de la batterie...</li>
        <li>connectivité 4g, BT, modem USB...</li>
        <li>clavier physique !...</li>
        <li>"mono-tâche by design"...</li>
      </ul>
    </td>
    <td>
      <ul>
        <li>mais lent (KaiOS ?)</li>
        <li>avec import galère de contacts</li>
        <li>avec des bugs de T9</li>
        <li>mais un "privacy by design" inexistant ?!</li>
      </ul>
    </td>
  </tr>
</table>

Je ne pense pas pouvoir dire que je suis un convaincu de KaiOS.

Mais clairement, ce téléphone, et également KaiOS ont de belles cartes à jouer.
La détox est bien là, on passe beaucoup moins de temps sur son téléphone,
et la batterie dure bien plus longtemps que sur les smartphones.
J'apprécie énormément ce dernier point.

**Après quelques mois d'utilisation, je suis pourtant revenu à mon Galaxy A3.**

Mes usages du quotidien restent basiques, mais ne sont pas toujours possibles
avec ce téléphone : horaires de train, covoiturage, comptes bancaires principalement.
Et après migration sur un OS moins aspirateur de données
(je parle de [/e/](https://e.foundation/), l'OS basé sur LineageOS déGooglisé)
la batterie tient mieux, et je peux utiliser mes applications "quotidiennes".

J'ai bon espoir de revenir sur KaiOS,
quitte à développer les applications manquantes !
**J'ai l'intention de faire l'acquisition du Nokia 800 Tough**, 
qui promet une durée de vie encore plus importante côté batterie !
Il s'agit pour moi d'avoir un téléphone en mode baroudeur,
que je peux emmener sans avoir trop peur de le faire tomber ou autre.

Pour le reste, le **Nokia 8110 4G est un téléphone efficace, séduisant, et pas très/trop cher**, (allez voir d'occasion).
Je le recommande pour des usages basiques (téléphone, mails, messages, gps sans guidage).
Pour les photos / vidéos, équipez vous d'un vrai appareil photo.
Le mono-tâche est vraiment bien, vous verrez !

Nous devons cependant garder en tête que ce téléphone 
est "logiquement" un **aspirateur à données**.
Même si la majeure partie du code source de KaiOS
est disponible sur [github](https://github.com/kaiostech/),
les applications type Google ou Facebook ne sont pas réputées
pour être respectueuses de la vie privée des gens.

**J'apprécierai que le retour de Mozilla sur ce projet
puisse amener toutes leurs valeurs sur le respect de la vie privée
au sein de KaiOS...**

![Vues du Nokia 8110 4G](/images/uploads/nokia_8110_4g_yellow.jpg "Vues du Nokia 8110 4G")