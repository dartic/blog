---
title: Comment écrire à nouveau sur sa clé USB sous Linux quand elle est en mode
  "lecture-seule"
cover: /images/uploads/usb-key-1212110_960_720.jpg
cover_color: ""
description: Quand vous n'arrivez plus à écrire sur votre clé USB sous Linux...
  voici comment remettre en état votre clé USB !
tags:
  - usb sticks
  - readonly
  - linux
  - dosfsck
date: 2020-05-07T20:09:45.841Z
is_published: true
layout: LayoutPost
sidebar: true
author: Mathieu DARTIGUES
---
Cela arrive de temps en temps.

Vous utilisez votre clé USB, parfois entre Linux et Windows, sans souci, en lecture, en écriture, jusqu'au jour où... PAF ! vous ne pouvez plus écrire sur votre clé USB sous Linux. Parfois, vous avez aussi une erreur de copie sur votre clé, et elle se bloque ainsi en lecture seule.

Le système positionne la clé en lecture seule car il détecte des anomalies. Ainsi, on peut récupérer les données sans écrire à nouveau, car le disque pourrait être considéré comme fragile, corrompu, ou capable de corrompre des données qu'on écrirait dessus.

Avant de formater votre clé USB, et de perdre toutes vos données,
il peut être intéressant de tester en ligne de commande quelques petites instructions.

## Méthodo

Voici ce qui marche pour moi, avec une clé USB formatée en type de partition `msdos` ou `fat`.
Comme je peux m'en servir sur Windows, j'utilise ce type de partition.

Je le mets ici, car je ne me souviens jamais et à chaque fois je dois reparcourir le web pour retrouver la solution.

1. lister les montage de notre machine avec [`df`](https://linux.die.net/man/1/df), et on repère là où est montée notre clé. Ici, le système est `/dev/sdb1` et il est monté sur `/media/mdartic/USBKEY`

```sh
mdartic@arietty:/media/mdartic$ df -Th
Sys. de fichiers Type     Taille Utilisé Dispo Uti% Monté sur
udev             devtmpfs   3,9G       0  3,9G   0% /dev
tmpfs            tmpfs      788M    1,7M  786M   1% /run
/dev/sda1        ext4       458G    421G   15G  97% /
tmpfs            tmpfs      3,9G    121M  3,8G   4% /dev/shm
tmpfs            tmpfs      5,0M    4,0K  5,0M   1% /run/lock
tmpfs            tmpfs      3,9G       0  3,9G   0% /sys/fs/cgroup
/dev/loop1       squashfs   398M    398M     0 100% /snap/redis-desktop-manager/335
/dev/loop0       squashfs    94M     94M     0 100% /snap/core/8935
/dev/loop2       squashfs    94M     94M     0 100% /snap/core/9066
tmpfs            tmpfs      788M     84K  788M   1% /run/user/1000
/dev/sdb1        vfat        29G     27G  2,3G  93% /media/mdartic/USBKEY
```

2. démonter notre clé avec [`umount`](https://linux.die.net/man/8/umount)
en reprenant le point de montage précédent `/media/mdartic/USBKEY`

```sh
mdartic@arietty:/media/mdartic$ umount /media/mdartic/USBKEY
```

3. réparer *automagiquement* le système de fichiers de la clé [`dosfck`](https://linux.die.net/man/8/dosfsck)

```sh
mdartic@arietty:/media/mdartic$ sudo dosfsck -a /dev/sdb1
fsck.fat 4.1 (2017-01-24)
0x41: Dirty bit is set. Fs was not properly unmounted and some data may be corrupt.
 Automatically removing dirty bit.
There are differences between boot sector and its backup.
This is mostly harmless. Differences: (offset:original/backup)
  71:55/43, 72:53/4c, 73:42/c3, 74:4b/a9, 75:45/20, 76:59/55, 77:20/53
  , 78:20/42
  Not automatically fixing this.
/*****/*****
 Start does point to root directory. Deleting dir. 
Reclaimed 2 unused clusters (32768 bytes) in 2 chains.
Performing changes.
/dev/sdb1: 69 files, 1730300/1875947 clusters
```

Ici, on peut constater qu'une anomalie n'a pas été corrigée automatiquement.

En utilisant la commande `sudo dosfsck /dev/sdb1`
nous pourrons passer manuellement sur chaque incohérence trouvée,
et appliquer une correction adaptée.

4. débrancher / rebrancher la clé, physiquement parlant

5. vérifier l'écriture d'un fichier sur ce système

```sh
mdartic@arietty:/media/mdartic$ touch /media/mdartic/USBKEY/test
```

Normalement, vous pouvez écrire à nouveau sur votre clé !

### Sources

* [Stack Exchange](https://unix.stackexchange.com/questions/216152/usb-disk-read-only-cannot-format-turn-off-write-protection)
* [Ask Ubuntu](https://askubuntu.com/questions/563764/usb-devices-showing-as-read-only)
* Crédit image de couverture : [jackmac34 sur pixabay](https://pixabay.com/photos/usb-key-memory-computer-digital-1212110/)