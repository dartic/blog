---
title: La mad'zette du 23 Décembre 2018
author: Mathieu DARTIGUES
tags:
  - veille
  - madzette
categories:
  - veille
date: 2018-12-23
layout: LayoutPost
is_published: true
sidebar: true
description: |
  Cette semaine, 7ème édition (17/12 > 23/12),
  (et dernière de 2018)
  avec #21lessonsforthe21stcentury de Noah Harari,
  beaucoup de #JavaScript
  (async/await, try/catch, prototypage),
  du #CodeGolf, de l'accessibilité,
  et toujours du #VueJS !
---

Hello à tou.te.s, la mad'zette recense quelques infos triées de ma veille quotidienne.<br>
Articles parus dans la semaine, ou pas, pas forcément 100% techniques, mais aussi politiques, culturels,...
avec potentiellement des émissions à voir ou revoir !

Bonne lecture et bonnes fêtes !

## Culture

Cette semaine je vous invite à découvrir le dernier livre de [Yuval Noah Harari](https://fr.wikipedia.org/wiki/Yuval_Noah_Harari) :

<img src='https://images.epagine.fr/030/9782226436030_1_75.jpg' style='max-width: 500px'>

Yuval Noah Harari est un historien israëlien,
connu dans le monde pour ses deux livres **Sapiens** (le passé de l'homme) et **Homo Deus** (son avenir),
traduits dans plusieurs dizaines de langues.<br>
Dans son dernier livre **21 leçons pour le XXIè siècle**,
Yuval Noah Harari se concentre sur le présent, et le très court terme.<br>
À travers les problèmes de notre époque, l'auteur déchiffre la réalité,
et propose sa vision, une vision moderne et éclairée sur la complexité
de notre monde : mondialisation, grandes firmes, religions, risques technologiques,...

Vous pouvez retrouver cet ouvrage à la librairie **Ombres Blanches** (librairie Toulousaine),
où vous pouvez même le commander [sur leur site web](https://www.ombres-blanches.fr/societe/autres/livre/21-lecons-pour-le-xxie-siecle/yuval-noah-harari/9782226436030.html).

## Technique

### JavaScript

L'année 2018 se termine, et le monde du Front-end a (encore) bien progressé cette année.<br>
[**Petite rétrospective**](https://levelup.gitconnected.com/a-recap-of-frontend-development-in-2018-715724c9441d)
pour lister toutes ces [r]évolutions, sur **WebAssembly**, **React** ou encore **VueJS**.

En s'inspirant du langage *Go*,
codebits propose de [faciliter la gestion des **try/catch** dans nos **async/await**](https://blog.grossman.io/how-to-write-async-await-without-try-catch-blocks-in-javascript/).
Sur la gestion des erreurs, *Lukas Gisder-Dubé* a écrit un
[**guide sur la gestion des erreurs en JS**](https://levelup.gitconnected.com/the-definite-guide-to-handling-errors-gracefully-in-javascript-58424d9c60e6).

Plus globalement, *JavaScript* est parfois mal compris,
notamment cette façon de faire de l'objet avec le *prototypage*.
*Sean Maxwell* de *gitconnected* essaie d'[expliquer simplement ce pattern](https://levelup.gitconnected.com/the-javascript-object-paradigm-and-prototypes-explained-simply-e9cb9eaa49aa).

### Code golf

L'écriture de code n'est pas toujours un exercice facile.
Arriver à faire faire à du code ce que l'on souhaite est déjà un bon objectif.
Arriver à le faire en le moins de ligne possible en est un autre.
C'est un exercice récréatif, appelé [*code-golf*](https://en.wikipedia.org/wiki/Code_golf).
Vous pouvez trouver tout un tas d'exercice sur [code-colf.io](https://code-golf.io/).
Attention à conserver... la maintenabilité du code !!

Voici un exercice réalisé par *lesscake* sur
[l'animation d'un carré noir dans un Canvas](https://www.lesscake.com/code-golf-javascript).
(il/elle a aussi écrit un tutoriel pour [Phaser](https://www.lesscake.com/phaser-game-tutorial)
framework pour écrire des jeux en JS)

### Accessibilité

*Chris Ashton* de *Smashing Magazine* fait un retour d'expérience
sur l'utilisation d'un lecteur d'écran.
[**Témoignage sur l'accessibilité**](https://www.smashingmagazine.com/2018/12/voiceover-screen-reader-web-apps/),
à travers *YouTube, BBC, Facebook et Amazon*.


### VueJS

*TypeScript* se retrouve de plus en plus dans l'écosystème *JavaScript*.
Le module [**vuex-class-modules**](https://github.com/gertqin/vuex-class-modules)
permet d'utiliser des décorateurs au sein de votre store *Vuex*,
pour identifer les *mutations*, *actions* et le *store* lui-même.

### Vue vs React [vs Angular]

![](https://cdn-images-1.medium.com/max/1000/1*xxjRGwOufnL34eEUzNS2Tw.jpeg)
Sans vouloir entretenir une guerre des frameworks qui n'en finirait pas,
*Joe Podwys* a écrit une série d'articles sur la réflexion qu'il a mené
dans son entreprise pour choisir un framework.<br>
Pour connaître plutôt bien *React* et *VueJS*,
je trouve son approche plutôt bien argumentée, sincère et même objective.
Prenez le temps de tout lire, c'est pas si long, et ça éclaire beaucoup.<br>
Ça s'appelle [**Vue vs React**](https://medium.com/@joe.podwys/vue-vs-react-part-0-thinning-the-herd-2ef8f27310dc),
et il y a même un peu d'*Angular* dedans :-).

<br>
<br>

![That's all folks !](../../assets/thats-all-folks.jpg)


