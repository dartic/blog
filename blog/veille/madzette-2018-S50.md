---
title: La mad'zette du 16 Décembre 2018
author: Mathieu DARTIGUES
tags:
  - veille
  - madzette
categories:
  - veille
date: 2018-12-16
layout: LayoutPost
is_published: true
sidebar: true
description: |
  Cette semaine, 6ème édition (10/12 > 16/12),
  avec des #FemmesDeTalent, du #Fairphone,
  #Firefox 64, #Nextcloud 15, #Loopback swagger,
  et bien sûr du #VueJS !
---

Hello à tou.te.s, la mad'zette recense quelques infos triées de ma veille quotidienne.

Articles parus dans la semaine, ou pas, pas forcément 100% techniques, mais aussi politiques, culturels,...
avec potentiellement des émissions à voir ou revoir !

Bonne lecture !

## Société

Cette semaine, focus sur **Fairphone** qui sécurise 7 millions d'€
et poursuit sa croissance (maîtrisée et respectueuse au mieux de l'environnement).
La nouvelle CEO **Eva** nous explique tout ça dans son premier article [à lire ici](https://www.fairphone.com/en/2018/12/11/hitting-the-ground-running-with-new-investments-and-a-new-ceo/).

Il est aussi probable que le **Fairphone**
[devienne le smartphone 'fer de lance'](https://forum.fairphone.com/t/poll-what-os-es-do-you-want-to-run-on-your-next-fairphone-fp3-or-later/44519)
du projet [**/e/**](https://e.foundation/?lang=fr) de **Gaël DUVAL**. À suivre !


## Sport

**Championnes de l'Euro 2018 !!!**

On ne pouvait pas rêver mieux pour cette équipe si talentueuse.

**L'équipe de France de Handball** a montré cette semaine une très jolie palette de couleurs...

Une défense aggressive et volumineuse quand Béatrice EDWIGE se transforme en rempart infranchissable,
et une attaque digne des **Experts** quand Estelle NZE MINKO et Oriane KANOR se transforment en Air France (Daniel NARCISSE)
pour atomiser les buts adverses...

Encore bravo pour cette très belle image renvoyée par des joueuses de talent,
respectueuse de l'adversaire et des arbitres.

<iframe border=0 frameborder=0 height=850 width=550
 src="https://twitframe.com/show?url=https%3A%2F%2Ftwitter.com%2Fmdartic%2Fstatus%2F1074374268786499585?ref_src=twsrc%5Etfw"></iframe>


## Technique

### Firefox 64

**Firefox** est sorti en version 64,
avec un [lot de nouveautés](https://hacks.mozilla.org/2018/12/firefox-64-released/)
dont la sélection d'onglets multiples,
l'amélioration des DevTools (affichage du ratio de contraste A11Y),
et le support de WebVR 1.1 pour Mac.

À l'heure où Microsoft change de moteur de rendu pour Blink (Chromium),
les équipes de Mozilla poursuivent leur route en toute liberté.

[Essayez Firefox !](https://www.mozilla.org/fr/firefox/new/) (c'est l'adopter !)

### Nextcloud 15

**Nextcloud 15** est sorti pour cette fin d'année
et [amène son lot de nouveautés](https://nextcloud.com/blog/nextcloud-15-goes-social-enforces-2fa-and-gives-you-a-new-generation-real-time-document-editing/).

Nextcloud est une solution libre de stockage / partage de données / fichier.
Un cloud privé.

Nextcloud renforce sa stratégie de gestion des données
en respectant le standard ActivityPub.
Vous pouvez désormais suivre et publier sur le réseau Mastodon !

### Loopback

[**Loopback**](http://v4.loopback.io/) est un framework Node.js pour construire des API.
La version 4 est sortie récemment, en Octobre dernier.
Réécrit entièrement en TypeScript,
le framework gère à la fois du RESTful mais aussi du GraphQL,
tout en étant compatible avec différents moteurs de base de données.

Désormais, vous pourrez retrouver l'[UI Swagger](https://swagger.io/tools/swagger-ui/)
pour explorer l'API en cours de construction.
Miroslav BAJTOS [explique sa démarche](https://strongloop.com/strongblog/how-we-built-a-self-hosted-rest-api-explorer/).

## Replay

Cette semaine, replay technique autour de ... **VueJS** !

Lors du DevFest 2018 à Toulouse,
Nicolas DECOSTER & Juliane BLIER ont fait une conf sur un remake de Pokémon, en VueJS.

Très pédagogues, je vous invite à les regarder si vous êtes intéressés de près ou de loin à VueJS.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/TDgln192b-Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

C'est tout pour cette semaine !

