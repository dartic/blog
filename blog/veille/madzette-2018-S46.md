---
title: La mad'zette du 18 Novembre 2018
author: Mathieu DARTIGUES
tags:
  - veille
  - madzette
categories:
  - veille
date: 2018-11-17
layout: LayoutPost
is_published: true
sidebar: true
description: |
  Cette semaine, 2ème édition (12/11 > 18/11), avec des élans d'humanité,
  de la psychologie, un peu de boue originelle,
  toujours un peu de technique, et un sujet de société
  sur la gestion de nos aînés.
---

Hello à tou.te.s, voici la deuxième édition de ma veille quotidienne.

Articles parus dans la semaine, ou pas, pas forcément 100% techniques, mais aussi politiques, culturels,...
avec potentiellement des émissions à voir ou revoir !

Bonne lecture !

## Société

**L'Iran permet aux femmes de venir voir des matchs de foot.**
La loi évolue, et les mentalités semblent être en phase, du moins à Téhéran [où les hommes applaudissent les femmes](https://www.francetvinfo.fr/replay-radio/la-medaille-du-jour/la-medaille-du-jour-un-millier-d-iraniennes-assistent-a-un-match-de-foot-a-teheran-et-se-font-applaudir-par-les-hommes_3012499.html
).

Parfois, les mentalités n'évoluent pas aussi vite,
avec par exemple [les tunisiens noirs qui espèrent plus d'égalité](https://www.liberation.fr/planete/2018/11/13/face-au-racisme-profondement-ancre-les-tunisiens-noirs-esperent-plus-d-egalite_1691765
)
après qu'une **loi pénalisant le racisme ait été adoptée**. La loi évolue, les mentalités ne sont pas toujours en phase.

En France, la colère gronde, chez les enseignants, et les automobilistes.
[Tristan Nitot nous livre son point de vue](https://standblog.org/blog/post/2018/11/10/Comprendre-la-colere-des-automobilistes
)
avec une vision psychologique issue d'un parallèle avec les 5 étapes du deuil.
Au delà de la position défendue par Tristan, **les commentaires sont tout aussi intéressants**.

## Culture

La recherche de l'origine de la vie sur terre progresse.
Le [CNRS détaille tout cela dans un communiqué](http://www2.cnrs.fr/presse/communique/5738.htm),
et [Next Inpact](https://www.nextinpact.com/brief/origine-de-la-vie-sur-terre---les-premieres-briques-observees-dans-des-roches-oceaniques-profondes-6436.htm
) nous résume cela dans une brève.
En bref, **on viendrait de la boue**. Passionnant !

Côté jeux vidéos, Gamekult et son émission **loop**
nous détaillent [les interfaces des jeux vidéos](https://www.gamekult.com/emission/vos-jeux-preferes-vivent-et-meurent-a-cause-de-leurs-interfaces-3050811129.html
). Si vous n'êtes pas abonnés GK Premium, utilisez ce code : **`Y0SWCI`**. (vu qu'il n'y a pas grand monde sur madz.fr - pour l'instant - sûrement qu'il marche !)

## Technique

**Smashing Magazine** détaille dans [un tweet](https://twitter.com/smashingmag/status/1062263165067685888)
plusieurs démos concernant l'animatin en Front,
avec du React, Vue.js et Angular. Merci Sarah Drasner !

**alsacréations** s'est déplacé à *dotJS* et *dotCSS*,
conférences sur le JS et le CSS sur Paris.
[Rodolphe a publié un résumé](https://www.alsacreations.com/actu/lire/1777-dotJS-2018.html).
Je retiendrais surtout les stats du [**State of JS**](https://stateofjs.com/) de Sacha Greif
qui mettent en relief React, largement plébiscité par ses dévs,
et Vue.js, dont la popularité ne cesse de grimper, au détriment d'Angular ?

L'autre gagnant est aussi **TypeScript**, poussé par minidoux (MicroSoft),
et [voici un article illustrant du typage sans forcément utiliser TypeScript](https://medium.com/@znck/type-vue-without-typescript-b2b49210f0b).
L'auteur utilise Vue.js, et en utilisant la balise `@type`,
l'intellisense de VS Code comprend plus facilement les types des données uilisés.

Cette semaine se déroulait la conférence [**VueToronto**](https://vuetoronto.com/).
L'occasion pour le créateur de Vue.js d'exposer les évolutions
attendues pour la 3ème mouture de cette librairie de composant.
Retrouvez [ses slides ici](https://docs.google.com/presentation/d/1yhPGyhQrJcpJI2ZFvBme3pGKaGNiLi709c37svivv0o/present?token=AC4w5VgGHPQy8RT9tEyvzCVlxMv5fLrmzw%3A1542578005454&includes_info_params=1#slide=id.p) et [un résumé par Gregg Polack](https://medium.com/vue-mastery/evan-you-previews-vue-js-3-0-ab063dec3547),
membre de la Core Team.

## Replay

Cette semaine, **France 5 a diffusé un reportage sur les EHPAD**,
les conditions d'exercice des professionnels mais aussi
les conditions de vie des personnes âgées, parfois dépendantes,
souvent vulnérables. [Revoir l'émission 'Maisons de santé, un système à bout de souffle'](https://www.france.tv/france-5/enquete-de-sante/782161-maisons-de-retraite-un-systeme-a-bout-de-souffle.html)

Si vous souhaitez vous investir sur cette cause,
vous pouvez échanger sur [make.org](https://make.org/?utm_source=madz.fr&utm_campaign=aines#/FR/consultation/aines/consultation).
N'hésitez pas.


C'est tout pour cette semaine !

