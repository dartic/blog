---
title: La mad'zette du 25 Novembre 2018
author: Mathieu DARTIGUES
tags:
  - veille
  - madzette
categories:
  - veille
date: 2018-11-25
layout: LayoutPost
is_published: true
sidebar: true
description: |
  Cette semaine, 3ème édition (19/11 > 25/11), avec les 50 ans du SAMU,
  les résultats du State of JS 2018, Big Flo & Oli, et des vidéos
  sur la Caste... une oligarchie au pouvoir.
---

Hello à tou.te.s, voici la troisième édition de ma veille quotidienne.

Articles parus dans la semaine, ou pas, pas forcément 100% techniques, mais aussi politiques, culturels,...
avec potentiellement des émissions à voir ou revoir !

Bonne lecture !

## Société

Lundi, **le SAMU fêtait ses 50 ans !**

<iframe src="https://www.linkedin.com/embed/feed/update/urn:li:share:6470602998853828608" allowfullscreen="" width="504" height="558" frameborder="0"></iframe>

Le SAMU, invention toulousaine du Professeur Louis Lareng,
date de 1968. Un joli exemple de désobéissance civile (il a failli passer en conseil de discipline),
avec l'idée simple de "ramener le médecin au pied de l'arbre".
Son parcours a été exemplaire (à mon sens...). Pour un Haut-Pyrénéen,
il a réussi à gravir les échelons politiques jusqu'à l'Assemblée Nationale
pour faire adopter son idée de SAMU à travers la loi du 6 Février 1986, dite **loi Lareng**...

L'image renvoyée par les équipes du SAMU, composé de femmes et hommes avec des valeurs humaines,
est une des rares images positives de notre société à être médiatisée.
Bravo aux équipes du SAMU, du SAMU 31,
[et de ses dirigeants](https://www.ladepeche.fr/article/2018/11/19/2909190-le-samu-c-est-eux.html).
Que l'histoire se répète et s'améliore.

Si d'aventure vous passiez dans les environs de Gaillac (Tarn),
n'hésitez pas à vous arrêter goûter les bouteilles du domaine [**Les Guiraudets**](http://www.terroirdelagrave.fr/2domaines.html#2).
Vous y rencontrerez peut-être l'actuel directeur du SAMU 31,
[Vincent Bounes](https://www.lejournaltoulousain.fr/societe/vincent-bounes-dirige-le-samu-31-une-institution-qui-fete-ses-50-ans-60294),
médecin-viticulteur autant attaché à ses équipes que son terroir ! Et, croyez moi sur parole, vous ne serez pas déçu par les différents vins qu'ils produisent...

## Culture

Cette semaine, les chanteurs toulousains Bigflo & Oli
ont sorti leur dernier album **La vie de rêve**.
Album très agréable, avec des points de vue sur notre société,
sur leur famille, et le style B&O.
N'hésitez pas à [écouter / commander leur dernier opus...](https://bigfloetoli.lnk.to/LaVieDeReve)

<iframe border=0 frameborder=0 height=620 width=550
 src="https://twitframe.com/show?url=https%3A%2F%2Ftwitter.com%2Fbigfloetoli%2Fstatus%2F1065752588090572801"></iframe>

Un trailer pour le nouveau Roi Lion vient d'être publié par Disney le 22 Novembre dernier... Disney continue de recycler ses classiques,
et le moins qu'on puisse dire, c'est que le trailer en jette !
Ce remake sera dirigié par [Jon Favreau](https://www.imdb.com/name/nm0269463/?ref_=tt_ov_dr),
qui a aussi réalisé le remake du Livre de la Jungle, et a beaucoup travaillé sur les Avengers, Iron Man, ...

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/4CbLXeGSDxg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Technique

Après une petite entrevue semaine dernière des résultats
du State of JS, [les résultats sont désormais publics](https://2018.stateofjs.com).
Bravo à Sacha Greif & son équipe pour tout ce travail !
La stack JS bouge peu, mais on voit sortir du lot des outils
comme **Storybook** (utilisé pour concevoir des composants), **Jest**
et aussi l'évolution de la couche d'accès aux données avec **GraphQL** et **Apollo**.

Les résultats semblent indiquer un fléchissement de l'intérêt de la communauté pour **Angular**.
Une vidéo postée par **Angular Firebase** essaie de démontrer un autre point de vue.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/UnEPBQvkNrg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Les résultats du State of JS sont le reflet de [tous les développeurs qui ont répondu au sondage](https://medium.freecodecamp.org/three-controversial-charts-from-the-state-of-js-2018-ec9dda45749).
Mieux que rien, ils ne sont pas *encore* représentatifs de l'ensemble des développeurs dans le monde.

## Replay

**La Caste**, c'est l'idée d'une équipe d'élite de notre pays,
ne répondant pas nécessairement aux valeurs républicaines.
Mais qui a un pouvoir énorme, puisqu'à la tête du pays.
Cette idée a été proposée par Laurent Mauduid (journaliste à Mediapart)
à travers son livre **La Caste, main basse sur l'état**.
Vous pourrez retrouver [quelques questions / réponses posées par **Pascal Boniface**]
(https://blogs.mediapart.fr/pascalboniface/blog/221018/la-caste-4-questions-laurent-mauduit),
ou une interview à travers Le Media par Aude Lancelin :

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/2mO0DN3tsAs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Cette semaine, le rédac chef du [journal Fakir](https://www.fakirpresse.info/) François Ruffin
(mais surtout député à l'Assemblée Nationale) a aussi discuté sur ce sujet,
en invitant Monique Pinçon-Charlot, Denis Robert et Laurent Mauduid.
Discussions et sujet intéressants. À revoir :

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/5svq2j34jVY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



C'est tout pour cette semaine !
