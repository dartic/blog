---
title: La mad'zette, première !
author: Mathieu DARTIGUES
tags:
  - veille
  - madzette
  - football leaks
categories:
  - veille
date: 2018-11-10
layout: LayoutPost
is_published: true
sidebar: true
description: |
  Cette semaine, la 1ère édition (05/11 > 11/11), avec des leaks côté foot, du jeu vidéo, un grain de data privacy,
  pas mal d'articles techniques et un téléfilm sur un sujet de société.
---

Hello à tou.te.s, voici la première itération de ma veille quotidienne.

J'ai pensé que ce serait pas mal de recenser tous ces petits articles qui attirent mon attention,
pour ma mémoire déjà, et puis si ça peut en intéresser d'autres... !

Articles parus dans la semaine, ou pas, pas forcément 100% techniques, mais aussi politiques, culturels,...
avec potentiellement des émissions à voir ou revoir !

Bonne lecture !

## Société

* Sophia Aram, chroniqueuse sur France Inter nous fait un [billet poignant sur Asia Bibi](https://www.franceinter.fr/emissions/le-billet-de-sophia-aram/le-billet-de-sophia-aram-05-novembre-2018),
femme jugée pour blasphème au Pakistan, condamnée à la pendaison, mais acquittée... officiellement ?

* [Mediapart](https://www.mediapart.fr) et les journalistes de l'[EIC *(en)*](https://eic.network/) publient plusieurs articles sur les
[Football Leaks, saison 2](https://www.mediapart.fr/journal/france/dossier/football-leaks-saison-2).
Au menu, le dessous des clubs, des ligues, les caprices de grosses stars du foot,... ça ne va pas redorer le blason du foot...

* [La Chine déploie un système obligeant les joueurs à s'identifier](https://www.gamekult.com/actualite/en-chine-tencent-verifie-desormais-l-identite-reelle-de-ses-millions-de-joueurs-3050811053.html).
Merci Tencent Games. On en pense quoi ?

* Dans la même veine, on parle de la ['Datafication' pour nos enfants *(en)*](https://techcrunch.com/2018/11/09/children-are-being-datafied-before-weve-understood-the-risks-report-warns/)...Parlons en, pensons y, et protégeons les efficacement.


## Culture

* Bonne nouvelle ! [Le magazine JV (culture Jeu Vidéo) a réussi sa campagne Kickstarter !](https://www.kickstarter.com/projects/445812678/sauvez-le-magazine-jv)
L'aventure continue !

* À ce sujet, après le livre [Génération Jeu Vidéo - Années 80](http://www.jvlemag.com/produit/generation-jeu-video-annees-80/)
le nouvel épisode sur les Années 90 est bientôt fini... (source magazine JV #57)

* Le Nintendo Switch Online continue d'enrichir son offre NES à partir du 14/11 avec [3 nouveaux jeux](https://twitter.com/NintendoFrance/status/1060093781389836288) : Metroid, Mighty Bomb Jack et TwinBee. Metroid premier du nom, épisode fondateur de la série est jouable depuis votre Switch !

* Nous fêtons les 100 ans de l'armistice 1918, et le jeu [**Soldats Inconnus : Mémoires de la grande guerre**](https://www.gamekult.com/actualite/child-of-light-et-soldats-inconnus-vont-sortir-sur-switch-3050808189.html)
est arrivé sur Switch ce 8 Novembre

## Technique

* Vous aimez Vue.js ? Moi aussi, alors voici [quelques tips de Chris Fritz](https://youtu.be/7YZ5DwlLSt8),
[ses slides](https://github.com/chrisvfritz/7-secret-patterns/blob/master/slides-2018-03-28-vueconfus-export.pdf)
et le [code source](https://github.com/chrisvfritz/7-secret-patterns). J'ai appris pas mal de choses !

* Je m'intéresse en ce moment au Code Sharing entre application web et natives. NativeScript en parle
à travers [un article](https://www.nativescript.org/blog/how-to-build-a-pwa-an-ios-app-and-an-android-app-from-one-codebase)
qui montre comment construire une PWA + une app iOS + une app Android avec une seule codebase.

* La [version 5.0 de NativeScript est sortie](https://www.nativescript.org/blog/nativescript-5.0-is-hot-out-of-the-oven) !
Au menu,
  * `tns preview` directement dans la CLI permet de visualiser son app en cours de dév, sans besoin de dépendance locale
  * nativescript-vue supporte tous les composants de NativeScript UI
  * le Hot Media Replacement (HMR) est implémenté !
  * la CLI avec `tns create` est beaucoup plus user friendly
  * et d'autres surprises

* Zeit, outil permettant de déployer facilement ses projets,
sort la [v2 de leur cli now](https://zeit.co/blog/now-2).
Sûrement mieux (?), mais avec des impacts de migration à prévoir,
et une gestion assez différente d'avant... Docker n'est plus supporté,
notion de [lambdas](https://zeit.co/docs/v2/deployments/concepts/lambdas)...

* Samsung poursuit ses recherches avec un Linux embarqué sur ses périphériques mobiles.
Une distro Ubuntu 16.04 est en phase de béta test. Vous pouvez vous enregistrer [par là](https://www.linuxondex.com/)

## Replay

Cette semaine, même si je ne regarde pas souvent la télévision, je dois avouer avoir été touché
par l'histoire bouleversante d'un ado, Jonathan Destin, qui il y a 7 ans s'est immolé par le feu, à l'âge de 16 ans.
[À revoir, à méditer, et à défendre](https://www.tf1.fr/tf1/le-jour-ou-j-ai-brule-mon-coeur)

