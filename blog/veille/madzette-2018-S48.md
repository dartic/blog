---
title: La mad'zette du 2 Décembre 2018
author: Mathieu DARTIGUES
tags:
  - veille
  - madzette
categories:
  - veille
date: 2018-12-02
layout: LayoutPost
is_published: true
sidebar: true
description: |
  Cette semaine, 4ème édition (26/11 > 02/12), avec du jaune,
  de l'investigation, un passage à la majorité,
  et de la technique avec du mobile + PWA.
---

Hello à tou.te.s, la mad'zette recense quelques infos triées de ma veille quotidienne.

Articles parus dans la semaine, ou pas, pas forcément 100% techniques, mais aussi politiques, culturels,...
avec potentiellement des émissions à voir ou revoir !

Bonne lecture !

## Société

Le mouvement des **gilets jaunes** se poursuit, et se durcit.

Sans rentrer dans les convictions de chacun, des débordements constatés,
et des comportements réprimables des gilets jaunes - et des autres -,
voici un résumé de la situation par François Ruffin de son département :

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/ODRQGbLtWas" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Côté santé, le **Consortium International des Journalistes d'Investigation** ([ICIJ](https://www.icij.org))
[publie une enquête gigantesque sur les dispositifs médicaux (implants par ex.)](https://www.icij.org/investigations/implant-files/).
Ce sera aussi le replay de cette semaine !

## Culture

Le site de jeu vidéo [GameKult](https://www.gamekult.com) fête ses 18 ans !
L'âge de la majorité... Vous ne les connaissez pas ?... zut...
C'est une équipe de journalistes (et pigistes) talentueux,
avec des dossiers, des émissions (retro island est top !) financés par les abonnés.
Cliquez, lisez, vous ne serez pas déçus ! (et abonnez vous !)

<iframe border=0 frameborder=0 height=620 width=550
 src="https://twitframe.com/show?url=https%3A%2F%2Ftwitter.com%2FGamekult%2Fstatus%2F1068769243557183488"></iframe>

## Technique

Côté développement mobile / PWA, le framework [**Ionic**](https://ionicframework.com/)
progresse à plusieurs niveaux.
Pour rappel, **Ionic** est un framework permettant de développer des web apps
et de les déployer sur les *stores Android & Apple* (app hybrides),
les *desktop* avec Electron ou encore le *web* avec les PWA.

**Ionic** est donc désormais compatible avec **Vue.js** (décidément, tout le monde s'y met...)
([annoncé à la VueConf de Toronto](https://blog.ionicframework.com/a-vue-from-ionic/)).
L'équipe de [**Modus Create**](https://moduscreate.com/blog/ionic-vue-modus-labs/)
est responsable de cette contribution très précieuse.

Un nouvel IDE nommé [**Ionic Studio**](https://ionicframework.com/studio)
a été publié pour faciliter le développement d'app Ionic.

Et 2019 nous réserve l'arrivée d'un remplaçant à [**Cordova**](https://cordova.apache.org/)
(couche d'abstraction permettant à Ionic de produire des apps hybrides sur Android & iOS),
que s'apelerio ["Capacitor"](https://capacitor.ionicframework.com/).
Plus de détail sur [cet article d'Ionic](https://blog.ionicframework.com/capacitor-in-2019-native-progressive-web-apps-for-all/)

Côté **Node.js**, un paquet npm ([event-stream](https://www.npmjs.com/package/event-stream))
utilisé massivement a été compromis.
[Explications de l'exploit](https://schneid.io/blog/event-stream-vulnerability-explained/)
et [explications de l'auteur du paquet (mais pas du code malicieux)](https://gist.github.com/dominictarr/9fd9c1024c94592bc7268d36b8d83b3a).
En bref, le libre a ses défauts.
Le maintien de projets libres est une grande difficulté pour beaucoup de *maintainers*,
car le temps est rarement rémunéré... et les exigences des utilisateurs en total décalage.


## Replay

Le replay de cette semaine est l'émission de Cash Investigation,
qui est sorti en quasi simultané de la publication de l'ICIJ,
au sujet des implants. Bon visionnage !

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/6mQZKAhZ-YQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


C'est tout pour cette semaine !
