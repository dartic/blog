---
title: La mad'zette du 9 Décembre 2018
author: Mathieu DARTIGUES
tags:
  - veille
  - madzette
categories:
  - veille
date: 2018-12-09
layout: LayoutPost
is_published: true
sidebar: true
description: |
  Cette semaine, 5ème édition (03/12 > 09/12),
  avec de l'intimidation, de la protection contre le chiffrement,
  du handball, les nouvelles freebox, du Chromium, du Vue.js,
  de l'IntelliCode et des Incas...
---

Hello à tou.te.s, la mad'zette recense quelques infos triées de ma veille quotidienne.

Articles parus dans la semaine, ou pas, pas forcément 100% techniques, mais aussi politiques, culturels,...
avec potentiellement des émissions à voir ou revoir !

Bonne lecture !

## Société

Pendant qu'en France, le mouvement des gilets jaunes se poursuit,
et que les infos principales relayées par les médias tv
sont les chiffres d'interpellation ou encore les dégâts provoqués par une minorité,

Pendant que la police - peu importe ses motifs - utilise des méthodes d'intimidiation
digne d'une autre époque et sème les graines de la colère dans les nouvelles générations,

<iframe border=0 frameborder=0 height=710 width=550
 src="https://twitframe.com/show?url=https%3A%2F%2Ftwitter.com%2FObs_Violences%2Fstatus%2F1070768467907919872"></iframe>

L'Australie a voté une loi de protection d'anti chiffrement, [la loi A&A (Assistance et Accès)](https://www.aph.gov.au/Parliamentary_Business/Bills_Legislation/Bills_Search_Results/Result?bId=r6195).
Cette loi va ainsi permettre au gouvernement Australien de demander l'assistance
des sociétés de tech pour casser le chiffrement des communications de certains devices,
voire même d'injecter des programmes malicieux sur ces mêmes périphériques.
Si Apple s'était fait remarqué pour avoir [refusé la demande du FBI](https://en.wikipedia.org/wiki/FBI%E2%80%93Apple_encryption_dispute)
sur l'accès aux clés de chiffrement, toute société du sol Australien risque de payer jusqu'à 7.3 millions de dollars
en cas de refus d'assistance...

## Sport

En ce moment se déroule l'**Euro de HandBall féminin, en France**,
du 29 Novembre au 16 Décembre.
La compétition se déroule en plusieurs phases
(tour préliminaire, tour principal, phase finale),
et malgré une défaite face à la Russie lors de leur premier match,
l'équipe de France a réalisé un très joli tour préliminaire
en montrant une défense très dynamique
(notamment une 1-5 bien gérée et deux super gardiennes avec Amandine LEYNAUD & Laura GLAUSER)
et une attaque construite où plusieurs joueuses se sont montrées décisives (Alexandra LACRABÈRE, Estelle NZE MINKO, Grace ZAADI).

Désormais dans le [tour principal](https://fra2018.ehf-euro.com/fr/programme-resultats/tournoi-final/tour-principal/groupe-i-nantes/),
l'équipe de France a fait match nul face à la Suède ce Dimanche (21 à 21), avec un réveil (tardif) dans les dix dernières minutes de la rencontre.
Prochain rendez-vous le 12 Décembre face à la Serbie, où la victoire sera indispensable pour poursuivre la compétition...

<iframe border=0 frameborder=0 height=800 width=550
 src="https://twitframe.com/show?url=https%3A%2F%2Ftwitter.com%2Fffhandball%2Fstatus%2F1071838649405321216"></iframe>

## Technique

Côté Technique, **Free a officialisé ses nouvelles offres**,
dont la [**freebox Delta**](https://www.free.fr/offre/freebox-delta/)
et la [**freebox One**](https://www.free.fr/offre/freebox-one).

Voilà le comparatif des différentes offres free à ce jour :
![Tableau comparatif des offres Freebox](https://cdn2.nextinpact.com/images/bd/news/medium-172650.png)

Free essaye de poursuivre son leadership sur les box,
sans se résumer à un simple fournisseur de modem-routeur-terminal-tv.
Vous trouverez une analyse plus poussée dans [l'article correspondant de Next Inpact](https://www.nextinpact.com/news/107372-freebox-delta-one-revolution-mini-4k-tarifs-conditions-et-tableau-comparatif.htm).

Côté navigateurs, **Microsoft** annonce que son **navigateur Edge va désormais [utiliser le moteur Chromium](https://blogs.windows.com/windowsexperience/2018/12/06/microsoft-edge-making-the-web-better-through-more-open-source-collaboration/)**.
Annoncé comme une collaboration plus poussée sur l'Open Source,
j'y vois un avantage, le moteur Chromium sera géré par des entités très motrices dans le domaine.
Pour le navigateur Firefox, les efforts pour s'aligner sur la performance et les rapidités d'évolutions
seront sûrement plus durs. En espérant qu'on ne retombe pas dans une ère ou un seul navigateur détenait les rênes.

Côté **Vue.js**, un **sondage** est **en cours** sur l'utilisation du framework :
[n'hésitez pas à y participer](https://stateofvuejs2019.typeform.com/to/m2nxN7).

Toujours sur **Vue.js**, si vous souhaitez optimiser votre configuration de votre IDE VS Code
pour coder plus facilement, avec des extensions comme Vetur / ESLint / Prettier,
[cet article](https://medium.com/vue-mastery/best-code-editor-for-vue-js-8b0d9cca6be)
recense les **bonnes configurations** à intégrer pour que les plugins parlent bien entre eux.

Encore sur **Vue.js**, l'équipe derrière [ReactiveSearch](https://opensource.appbase.io/reactivesearch/)
qui propose un kit UI pour ElasticSearch
vient de [publier une version Vue.js](https://medium.appbase.io/vue-js-components-for-building-search-uis-7b2a1b6fe159).
Tous les composants ne sont pas encore disponibles,
mais la communauté devrait sûrement progresser à ce niveau.

Côté **State of JS**, Sacha Greif vient de publier un article
concernant la traduction du sondage dans d'autres langues.
Retrouvez ses instructions [par ici](https://medium.com/@sachagreif/help-us-translate-the-state-of-javascript-2018-survey-results-1488efa525c1).

Last but not least, **Microsoft** a ajouté une nouvelle corde à son moteur IntelliSense.
Il s'agit de l'IA, avec
[IntelliCode](https://blogs.msdn.microsoft.com/visualstudio/2018/12/05/visual-studio-intellicode-supports-more-languages-and-learns-from-your-code/),
qui permet de proposer des suggestions liées au contexte.
Cette fonctionnalité est [accessible au sein de VS Code](https://blogs.msdn.microsoft.com/typescript/2018/12/05/intellicode-for-typescript-javascript/) à travers le plugin IntelliCode.

## Replay

Cette semaine, je vous propose de regarder un documentaire
de Science Grand Format sur [l'histoire de l'empire Inca](https://www.france.tv/france-5/science-grand-format/811521-l-histoire-de-l-empire-inca.html#xtref=acc_dir).

C'est tout pour cette semaine !
