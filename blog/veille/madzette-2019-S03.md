---
title: La mad'zette du 20 Janvier 2019
author: Mathieu DARTIGUES
tags:
  - veille
  - madzette
categories:
  - veille
date: 2019-01-20
layout: LayoutPost
is_published: true
sidebar: true
description: |
  Cette semaine, 1ère édition en retard pour 2019 ! (14/01 > 20/01),
  avec les #soldes vues par l'Ademe / Fraîches,
  le championnat de #handball19,
  un peu de #JavaScript,
  toujours du #VueJS,
  et un replay de #EnvoyerSpecial sur le #cacao + #glyphosate...
---

Hello à tou.te.s, la mad'zette recense quelques infos triées de ma veille quotidienne.<br>
Articles parus dans la semaine, ou pas, pas forcément 100% techniques, mais aussi politiques, culturels,...
avec potentiellement des émissions à voir ou revoir !


## Culture / Société / Sport

### Soldes !

En cette période de soldes, l'[Ademe](https://www.ademe.fr/) diffuse une vidéo de [Fraîches](https://www.facebook.com/fraichesminutebuzz)
sur l'impact de nos petites emplettes de vêtements !

<iframe border=0 frameborder=0 height=850 width=550
 src="https://twitframe.com/show?url=https%3A%2F%2Ftwitter.com%2FMin_Ecologie%2Fstatus%2F1082917851265200130?ref_src=twsrc%5Etfw"></iframe>

### Handball

Côté **sport**, le [championnat du monde de HandBall](http://www.handball19.com/)
se déroule simultanément en Allemagne et au Danemark.

L'équipe de France masculine effectue un joli parcours
(en tête de la première phase de poule et pour l'instant de la 2ème phase).
Ce dimanche (20 Janvier), la France rencontrait l'Islande
([31 - 22 pour la France](https://www.lequipe.fr/Handball/match/80845)).

L'entame de match a montré une équipe solide en défense,
et un Vincent Gérard (le gardien) dernier rempart infranchissable durant 10 minutes.
Et qui a été le meilleur buteur du match un court instant (2 buts !).
Élu meilleur joueur du match, il est un des éléments clés de cette équipe,
tout comme pouvait l'être Titi Omeyer en son temps.

![Vincent Gérard élu meilleur joueur du match France - Islande](https://pbs.twimg.com/media/DxYtP97WsAE3Wz0.jpg:large)

Je vous invite à suivre le match de ce Mercredi, 20h30 sur Bein Sports 1,
qui opposera la France à la Croatie. Match qui sera sûrement très appuyé.

### Jeux Vidéos

![Switch avec Mario Kart 8 Deluxe](https://d3isma7snj3lcx.cloudfront.net/optim/images/news/30/3050812787/avec-2-millions-de-switch-vendues-en-france-nintendo-atteint-son-objectif-a3cad2f3__930_300__0-326-1960-959.jpg)

Côté **Nintendo**, l'objectif de vente de la dernière pépite en date (la Switch)
a été atteint en France : il s'agissait d'en écouler 2 millions. La sortie
de Super Smash Bros Ultimate n'est pas innocente sur les chiffres de vente,
qui ne connaissent pas de ralentissement !! (à lire sur Gamekult
[ici](https://www.gamekult.com/actualite/avec-2-millions-de-switch-vendues-en-france-nintendo-atteint-son-objectif-3050812787.html)
et [là](https://www.gamekult.com/actualite/charts-japon-super-smash-bros-ultimate-ferme-une-annee-et-en-ouvre-une-autre-3050812765.html
))

## Technique

**GitHub** a officialisé en début d'année la
[création gratuite de repos privés](https://github.blog/2019-01-07-new-year-new-github/).

L'année dernière, **Gitlab** entrait dans la danse du Serverless
en [annonçant son intégration dans la version 11.6](https://about.gitlab.com/2018/12/11/introducing-gitlab-serverless/).

Si vous utilisez VS Code et Docker,
je suis repassé sur un très bon tuto pour faire fonctionner les deux ensembles.
Ça se passe sur [scotch.io](https://scotch.io/tutorials/docker-and-visual-studio-code).

### JavaScript

Côté **JavaScript**, [Rising Stars](https://risingstars.js.org/)
a sorti un bilan des stars GitHub sur l'année 2018.

Nous retrouvons les tendances du State of JS, à savoir que :
* VueJS est pour la troisème fois consécutive le projet le plus tendance (2016 / 2017 / 2018),
et désormais le framework le plus étoilé mais aussi le projet JavaScript #1
* Deno, le projet de Ryan Dahl (créateur de NodeJS) s'introduit à la 4ème place du TOP 10
* côté Mobile, React Native truste clairement la 1ère place de la popularité (+14.6k),
mais les 4 autres sur le podium sont des solutions compatibles VueJS (Weex, NativeScript, Quasar et Ionic)
* TypeScript perce en #1, avec le support de TS par Babel qui a dû aider

En parlant de TypeScript, l'équipe de Jest évalue la possibilité
de migrer la code base de Flow à TypeScript.
C'est à suivre [par ici](https://github.com/facebook/jest/pull/7554).


### VueJS

La [v2.6 entre en phase bêta](https://github.com/vuejs/vue/releases/tag/v2.6.0-beta.1),
et dans les nouveautés intéressantes,
Evan You [extrait la fonction qui transforme un objet en observable](https://github.com/vuejs/vue/commit/c50bbde41c4a1868a8a0b33df3238346840bd37c).

Ce qui me donne l'idée de mettre par écrit un peu plus précisément
un store 'allégé', que je présente dans mes différentes formations VueJS,
et qui serait une alternative pour Vuex. À suivre... ?!
(spoiler, ceci peut être une promesse de Gascon, ou de Normand, au choix)

Pendant que je finalise l'écriture de cette édition,
[Jen Looper](https://developer.telerik.com/author/jlooper/)
publie à l'instant un tweet concernant le Code Sharing
entre les apps mobiles (Android / iOS) et web grâce à NativeScript.
NativeScript permettait déjà de faire du Code Sharing
côté Angular, et c'était documenté, mais il manquait encore la partie VueJS...

Chose faite, a priori :

<iframe border=0 frameborder=0 height=750 width=550
 src="https://twitframe.com/show?url=https%3A%2F%2Ftwitter.com%2Fjenlooper%2Fstatus%2F1087108576483659782?ref_src=twsrc%5Etfw"></iframe>

## Replay

Le replay de cette semaine est autour de l'émission d'Envoyé Spécial
du 10 Janvier dernier, sur les enfants qui récoltent le Cacao...

<iframe border=0 frameborder=0 height=550 width=550
 src="https://twitframe.com/show?url=https%3A%2F%2Ftwitter.com%2FEnvoyeSpecial%2Fstatus%2F1083476614586007552?ref_src=twsrc%5Etfw"></iframe>

À revoir [sur le portail France TV](https://www.france.tv/france-2/envoye-special/850899-envoye-special.html).


C'est tout pour cette édition ! À la prochaine !

<br>

![That's all folks !](../../assets/thats-all-folks.jpg)
