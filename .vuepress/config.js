module.exports = {
  title: 'mad\'z home',
  description: 'Mathieu Dartigues, artisan-développeur web et papa-compagnon-citoyen',
  markdown: {
    lineNumbers: true
  },
  // permalink: '/:year/:month/:day/:slug',
  plugins: [
    [
      '@vuepress/blog',
      {
        sitemap: {
          hostname: 'https://www.madz.fr'
        },
        feed: {
          canonical_base: 'https://www.madz.fr',
         },
       },
    ],
    require('./plugins/netlify-cms.js'),
  ],
  themeConfig: {
    lastUpdated: 'Dernière mise à jour',
    nav: [
      { text: 'Blog', link: '/blog/' },
      { text: 'À propos', link: '/about/' },
    ],
  },
}
