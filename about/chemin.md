---
title: Parcours de vie
date: 2018-10-11
tags:
  - cv
  - association
  - professionnel
  - formation
---

# Chemin de vie

Voici un petit résumé de mon chemin de vie, associatif, professionnel et scolaire.

## Associatif

* (2018-xx) **CIS Fleurance** (Fleurance, Gers 32)
  * Centre d'Incendie et de Secours de Fleurance
  * sapeur-pompier volontaire

* (2017-xx) **Lomagne HandBall** (Fleurance, Gers 32)
  * Club de hand de Fleurance et alentours
  * joueur Senior Masculin sans poste attitré (niveau Département)
  * participation à la communication du club (affiches)

* (2013-2016) [**CIEL 31**](https://www.ciel31.fr) (Escalquens, Haute-Garonne 31)
  * Club Intercommunal Escalquens Labège (hand)
  * joueur Senior Masculin au poste d'ailier / demi-centre (niveau Département)
  * entraîneur de l'équipe 2

* (2004-2013) [**CRAHB**](http://crahb.net) (Ramonville, Haute-Garonne 31)
  * Castanet Ramonville Auzeville HandBall, club de la zone Sud-Est toulousaine
  * entraîneur jeune catégorie (-11F, -13F, -15F)
  * joueur Senior Masculin au poste d'ailier (niveau Département / Région)
  * membre du bureau (secrétariat)

* (2005-2012) **Podium Fiesta Loca** (Montréjeau, Haute-Garonne 31)
  * Animateur au sein d'une discomobile (2 podiums)
  * co-fondation de l'association
  * gestion des contrats de prestation
  * gestion du matériel
  * gestion de la trésorerie
  * formation de l'équipe (10 personnes au maximum)

* (2004-2011) [**Troubadours du Mont Royal**](https://www.facebook.com/Les-Troubadours-du-Mont-Royal-141229512559566/timeline/) (Montréjeau, Haute-Garonne 31)
  * danseur folklorique au sein d'un ballet Pyrénéen
  * membre du bureau

* (2005-2007) **Bureau des étudiants NTIE**
  * membre du bureau (trésorier)
  * organisation des weeks ends d'intégration

* (2002-2006) **Comité des fêtes du Boila** (Saint Laurent de Neste, Hautes-Pyrénées 65)
  * Bénévole puis président du Comité des fêtes
  * organisation des festivités
  * gestion de la trésorerie, des contrats, du GUSO
  * gestion de l'équipe du comité

## Entreprises & Projets

Vous pouvez retrouver cette partie mieux détaillée sur mon [profil Linked In](https://www.linkedin.com/in/mathieu-dartigues-b73a5937)

En quelques mots :
* (2016-xx) [ENSEEIHT](http://www.enseeiht.fr/fr/index.html) : enseignant JS au sein de la [formation FullStack](http://formation-fullstack.fr)
  * [VueJS](http://n7-vuejs.surge.sh/)
  * [JavaScript / Tooling / Tests / CI](http://n7-javascript.surge.sh/)

* (2016-xx) [Makina Corpus](https://makina-corpus.com) : développeur JS/TS et formateur React / Vue.js

* (2014-2016) [APSIDE](https://www.apside.com) : développeur, 2 projets principaux
  * [Paul Boyé Technologies](http://www.paul-boye.fr/) : ERP sur la gestion de la production de vêtements spécialisés
  * [ITCE (groupe Caisse d'Épargne)](https://www.it-ce.fr/) : framework front pour le projet MyWay (refonte du SI backoffice)

* (2006-2014) [ORU-MiP](https://www.orumip.fr) : développeur / chef d'équipe

* (2004-2006) [CHU Toulouse](https://www.chu-toulouse.fr) : développeur
  * [Centre de Consultation Médicale Maritime](https://www.chu-toulouse.fr/-centre-de-consultation-medicale-maritime-ccmm-)
    * Refonte du SI à travers un CMS (Xoops) et une base MySQL
    * Mise en place du reporting sous Business Object
  * [SAMU 31](https://www.chu-toulouse.fr/-samu-31-)
    * Conception / développement d'un outil de gestion des lits sur l'ensemble des unités du CHU, permet de connaître l'état des lits disponibles
    * Conception / développement d'un outil de gestion de catastrophe type AZF

## Formations

* **Études**
  * (2004-2007) [IUP Le Mirail, Université Toulouse II, Nouvelle Technologie de l'Informatique en Entreprise](https://mathsinfo.univ-tlse2.fr/accueil/navigation/formations/master-ice/), 2ème de promotion
  * (2002-2004) [IUT Paul Sabatier, Université Toulouse III, Informatique de Gestion](http://iut.ups-tlse.fr/dut-informatique-327044.kjsp), 2ème de promotion
  * (1999-2002) [Lycée général à Lannemezan](http://citescolaire-lannemezan.entmip.fr/lycee-michelet/) (Hautes Pyrénées, 65), BAC S SVT mention bien
  * école / collège à Saint Laurent de Neste (Hautes Pyrénées, 65)

* Autres
  * (2018-2019) Formation de Sapeur-Pompier Volontaire
  * (2004-2010) Formation d'entraîneur HandBall du niveau 1 (animation) au niveau 3 (régional, non finalisé)
  * (2008) Stage de Programmation Neuro Linguistique
