---
title: Parcours de vie
date: 2018-05-28
---

# À propos !

![Photo de Mathieu DARTIGUES](../assets/mdartic_photo.png)

Hello !

Voici quelques infos à mon sujet.

Je m'appelle **Mathieu DARTIGUES**, j'ai 34 ans et j'habite dans le Gers (32, Sud Ouest, Occitanie), à Fleurance.

Je vis avec une charmante demoiselle,
et nous essayons de rendre autonome nos 2 petits bambins qui ont 3 et 4 ans (et demi !).

Je suis développeur web, orienté Front (React & Vue.js principalement),
mais aussi back avec du Node.js (Loopback, FeathersJS, Strapi).
Ça fait de moi - je crois - un développeur JavaScript FullStack.

Je travaille dans une Entreprise qui respire le bon air du libre, [Makina Corpus](https://makina-corpus.com).

J'aime ce métier car j'aime créer à partir des outils numériques.
J'aime échanger et travailler avec d'autres personnes,
pour fabriquer un produit enrichi de visions différentes et complémentaires.
C'est un plaisir qui m'a envahi quand j'étais minot,
du temps où mon Papa, mon frère et moi écrivions des programmes de jeux vidéos en BASIC
et les faisions tourner ensuite sur [notre TO8](https://fr.wikipedia.org/wiki/Thomson_TO8)...

En parallèle de cette vie familiale / professionnelle,
j'essaye de m'investir localement, au sein du Centre d'Incendie et de Secours de Fleurance,
en tant que sapeur-pompier volontaire,
et profiter des installations sportives de Fleurance en jouant au handball dans le club local.

Vous pouvez me retrouver sur les réseaux sociaux :
* [twitter](https://twitter.com/mdartic)
* [github](https://github.com/mdartic)
* [gitlab](https://gitlab.com/mdartic)
* [linked in](https://www.linkedin.com/in/mathieu-dartigues-b73a5937)

Si vous souhaite découvrir un peu plus sur mon chemin, [c'est par ici](chemin.html).
